package ru.xiexed.mongodbdumper;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.mongodb.*;
import com.mongodb.util.JSON;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

/**
 * Created with IntelliJ IDEA.
 * User: nickl
 * Date: 13.04.13
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
public class Dumper {

    @Parameter(names = "-d", required = true, description = "Mongo database to use")
    String dbName;

    @Parameter(names = "-c", required = false, description = "Collection to use. You can specify \"-c\" multiple times")
    List<String> collNames = Collections.emptyList();

    @Parameter(names = "-p", required = false, description = "Collection name regexp to use. You can specify \"-p\" multiple times")
    List<String> collPatterns = Collections.emptyList();

    @Parameter(names = "-zip", description = "use zip compression")
    boolean zip = false;

    @Parameter(names = "--compression-level", description = "zip compression level -1 to 9")
    int compression = -1;

    @Parameter(names = "-v", description = "verbose")
    boolean verbose = false;

    @Parameter(names = "-emulate", description = "emulate")
    boolean emulate = false;

    @Parameter(names = "-bw", description = "bandwidth in bytes per second")
    double bandwidth = 0;

    @Parameter(names = "--host", description = "host (could be multiple)")
    List<String> host = asList("localhost");

    @Parameter(names = "--port", description = "port (could be multiple)")
    List<Integer> port = Collections.emptyList();

    @Parameter(names = "--primary", description = "prefer primary")
    boolean primary = false;

    @Parameter(
            names = {"-q", "--query"},
            description = "Json query in format {time:{$lte:{$date:\"2013-02-08T21:36:21Z\"},$gte:{$date:\"2013-01-08T21:36:21Z\"}}} " +
                    "in terminal it would be like \"{time:{'\\$lte':{'\\$date':'2015-01-01T00:00:00Z'}}}\" "
    )
    String query = null;

    @Parameter(
            names = {"-s", "--sort"},
            description = "sort query same as -q"
    )
    String sort = null;

    Mongo mongoClient;

    public static void main(String[] args) throws Exception {

        Dumper dumper = new Dumper();
        JCommander jCommander = new JCommander();
        try {
            jCommander.addObject(dumper);
            jCommander.parse(args);
            dumper.process();
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            jCommander.usage();
        }
    }

    public void process() throws Exception {

        if (collNames.isEmpty() && collPatterns.isEmpty()) {
            throw new ParameterException("'-c' or '-p' must be set");
        }

        ArrayList<ServerAddress> serverAddresses = new ArrayList<>();
        for (int i = 0; i < host.size(); i++) {
            serverAddresses.add(new ServerAddress(
                    host.get(i),
                    (port.size() > i) ? port.get(i) : 27017));
        }

        mongoClient = new MongoClient(serverAddresses);
        mongoClient.setReadPreference( primary ? ReadPreference.primaryPreferred() : ReadPreference.secondary());

        final Set<String> collectionNames = CollectionNamesGetter.getCollectionNames(mongoClient.getDB(dbName));

        OutputStream outputStream = System.out;
        if(emulate)
            outputStream = new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                }
            };

        if (bandwidth > 0)
            outputStream = new ThrottledOutputStream(outputStream, bandwidth);

        if (!zip) {
            if (collNames.size() != 1 && !collPatterns.isEmpty())
                throw new IllegalArgumentException("cant process multiple collections without zip");

            try (BufferedOutputStream out = new BufferedOutputStream(outputStream)) {
                writeCollectionToStream(out, collNames.get(0));
            }
        } else {

            List<Pattern> patternList = new ArrayList<>();
            for (String collName : collNames) {
                patternList.add(Pattern.compile(Pattern.quote(collName).replace("*", "\\E.*\\Q")));
            }

            for (String pattern : collPatterns) {
                patternList.add(Pattern.compile(pattern));
            }

            List<String> collNamesByPattern = new ArrayList<>();
            for (String collectionName : collectionNames) {
                for (Pattern pattern : patternList) {
                    if (pattern.matcher(collectionName).matches()) {
                        collNamesByPattern.add(collectionName);
                        break;
                    }
                }
            }

            if (verbose)
                System.err.println("colls to process = " + collNamesByPattern.size());
            int processed = 0;
            try (ZipArchiveOutputStream out = new ZipArchiveOutputStream(outputStream)) {
                out.setUseZip64(Zip64Mode.Always);
                if(compression != -1)
                    out.setLevel(compression);
                for (String collName : collNamesByPattern) {
                    if (verbose)
                        System.err.println("processing: " + processed + "/" + collNamesByPattern.size() + " \"" + collName + "\"");

                        ZipArchiveEntry archiveEntry = new ZipArchiveEntry(collName + ".bson");
                        out.putArchiveEntry(archiveEntry);
                        writeCollectionToStream(out, collName);
                        out.closeArchiveEntry();

                    processed++;
                }
            }
            if (verbose)
                System.err.println("processed: " + processed + "/" + collNamesByPattern.size());

        }

    }

    private void writeCollectionToStream(OutputStream out, String collName) throws IOException {
        DBCollection collection = mongoClient.getDB(dbName).getCollection(collName);
        collection.setDBDecoderFactory(new NoDecodingDecoder());
        BasicDBObject queryObject = new BasicDBObject();
        if (query != null) {
            queryObject = (BasicDBObject) JSON.parse(query);
            if(verbose)
                System.err.println("query:"+queryObject + " from param: " + query);
        }

        BasicDBObject sortObject = new BasicDBObject();
        if (sort != null) {
            sortObject = (BasicDBObject) JSON.parse(sort);
            if(verbose)
                System.err.println("sort:"+sortObject);
        }

        DBCursor dbCursor = collection.find(queryObject).sort(sortObject).batchSize(1000);
        while (dbCursor.hasNext())
            out.write((byte[]) dbCursor.next().get("!data"));
    }


}
