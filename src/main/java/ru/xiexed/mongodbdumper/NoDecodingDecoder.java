package ru.xiexed.mongodbdumper;

import com.mongodb.*;
import org.bson.io.Bits;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
* Created with IntelliJ IDEA.
* User: nickl
* Date: 13.04.13
* Time: 20:38
* To change this template use File | Settings | File Templates.
*/
class NoDecodingDecoder implements DBDecoderFactory {
    @Override
    public DBDecoder create() {
        return new LazyDBDecoder() {

            public DBObject decode(InputStream in, DBCollection collection) throws IOException {

                DataInputStream dataInputStream = new DataInputStream(in);

                int size = Bits.readInt(in);
                byte[] data = new byte[size];

                data[0] = (byte) ((0x000000FF & size) >> 0);
                data[1] = (byte) ((0x0000FF00 & size) >> 8);
                data[2] = (byte) ((0x00FF0000 & size) >> 16);
                data[3] = (byte) ((0xFF000000 & size) >> 24);

                dataInputStream.readFully(data, 4, size - 4);

                BasicDBObject object = new BasicDBObject();
                object.put("!data", data);
                return object;
            }

        };
    }
}
