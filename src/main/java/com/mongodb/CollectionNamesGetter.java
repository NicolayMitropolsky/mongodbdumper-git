package com.mongodb;

import java.util.*;

import static java.util.Arrays.asList;

/**
 * Created by nickl-mac on 31.12.15.
 */
public class CollectionNamesGetter {

    public static Set<String> getCollectionNames(DB db0) {

        DBApiLayer db = (DBApiLayer)db0;
            db.requestStart();
            try {
                List<String> collectionNames = new ArrayList<String>();
                if (db.isServerVersionAtLeast(asList(3, 0, 0))) {
                    CommandResult res = db.command(new BasicDBObject("listCollections", db.getName()));
                    if (!res.ok() && res.getCode() != 26) {
                        res.throwOnError();
                    } else {
                        QueryResultIterator iterator = new QueryResultIterator(res, db.getMongo(), 0, DefaultDBDecoder.FACTORY.create(),
                                res.getServerUsed());
                        try {
                            while (iterator.hasNext()) {
                                DBObject collectionInfo = iterator.next();
                                collectionNames.add(collectionInfo.get("name").toString());
                            }
                        } finally {
                            iterator.close();
                        }
                    }
                } else {
                    Iterator<DBObject> collections = db.getCollection("system.namespaces")
                            .find(new BasicDBObject());
                    for (; collections.hasNext();) {
                        String collectionName = collections.next().get("name").toString();
                        if (!collectionName.contains("$")) {
                            collectionNames.add(collectionName.substring(db.getName().length() + 1));
                        }
                    }
                }
                Collections.sort(collectionNames);
                return new LinkedHashSet<String>(collectionNames);
            } finally {
                db.requestDone();
            }
        }
}
